import {FC, useEffect, useState} from 'react'
import {get, getWithQueryString} from './core/_requests'

const Region: FC = () => {
  const [regionList, setRegionList] = useState<{name: string; id: number}[]>([])
  const [provinceList, setProvinceList] = useState<{name: string; id: number}[]>([])
  const [cityList, setCityList] = useState<{name: string; id: number}[]>([])
  const [idRegion, setIdRegion] = useState<number>()
  const [idProvince, setIdProvince] = useState<number>()

  const handleChangeRegion = (e: React.ChangeEvent<HTMLSelectElement>) => {
    const selectedId = parseInt(e.target.value, 10)
    setIdRegion(selectedId)
    setIdProvince(0)
  }

  const handleChangeProvince = (e: React.ChangeEvent<HTMLSelectElement>) => {
    const selectedId = parseInt(e.target.value, 10)
    setIdProvince(selectedId)
  }
  
  // SOAL ALGORITMA -------------------------------------------------------------------
  // function oddNumbers(l: number, r: number) {
  //   let result = []

  //   for (let i = l; i <= r; i++) {
  //     if (i % 2 !== 0) {
  //       result.push(i)
  //     }
  //   }

  //   return result
  // }

  // function no1() {
  //   const l = 0
  //   const r = 10
  //   const result = oddNumbers(l, r)
  //   console.log(result.join('\n') + '\n')
  // }

  // function fizzBuzz(n: number) {
  //   for (let i = 1; i <= n; i++) {
  //     if (i % 3 === 0 && i % 5 === 0) {
  //       console.log('FizzBuzz')
  //     } else if (i % 3 === 0) {
  //       console.log('Fizz')
  //     } else if (i % 5 === 0) {
  //       console.log('Buzz')
  //     } else {
  //       console.log(i)
  //     }
  //   }
  // }

  // function no2() {
  //   const n = 15
  //   fizzBuzz(n)
  // }

  // ----------------------------------------------------------------------------------

  useEffect(() => {
    const fetchData = async () => {
      try {
        const {data} = await get('islands')
        const result: Record<string, any> = data.data
        setRegionList(result.result)
      } catch (e) {
        console.log(e)
      }
    }

    let paramProvince = '!limit=99'
    if (idRegion) {
      paramProvince = `id_island=${idRegion}&!limit=99`
    }
    const fetchDataProvince = async () => {
      try {
        const {data} = await getWithQueryString('provinces', paramProvince)
        const result: Record<string, any> = data.data
        setProvinceList(result.result)
      } catch (e) {
        console.log(e)
      }
    }
    fetchDataProvince()

    let paramCity = '!limit=999'
    if (idProvince) {

      paramCity = `id_province=${idProvince}&!limit=99`
    }
    const fetchDataCity = async () => {
      try {
        const {data} = await getWithQueryString('cities', paramCity)
        const result: Record<string, any> = data.data
        setCityList(result.result)
      } catch (e) {
        console.log(e)
      }
    }
    fetchDataCity()
    fetchData()
    // HASIL SOAL ALGORITMA
    // no1();
    // no2();
  }, [idRegion, idProvince])

  return (
    <>
      <h4>Region</h4>
      <div>Total ({regionList.length})</div>
      <select
        name='status'
        data-control='select2'
        data-hide-search='true'
        className='form-select form-select-sm form-select-white w-300px'
        onChange={handleChangeRegion}
        value={idRegion}
      >
        <option key={-1} value={0}>
          select ...
        </option>
        {regionList.map((val, index) => {
          return (
            <option key={index} value={val.id}>
              {val.name}
            </option>
          )
        })}
      </select>

      <h4>Province</h4>
      <div>Total ({provinceList.length})</div>
      <select
        name='status'
        data-control='select2'
        data-hide-search='true'
        className='form-select form-select-sm form-select-white w-300px'
        onChange={handleChangeProvince}
        value={idProvince}
      >
        {idRegion ? (
          <>
            <option key={-1} value={ 0}>
              select ...
            </option>
            {provinceList.map((val, index) => {
              return (
                <option key={index} value={val.id}>
                  {val.name}
                </option>
              )
            })}
          </>
        ) : null}
      </select>

      <h4>City</h4>
      <div>Total ({cityList.length})</div>
      <select
        name='status'
        data-control='select2'
        data-hide-search='true'
        className='form-select form-select-sm form-select-white w-300px'
      >
        {idProvince
          ? cityList.map((val, index) => {
              return (
                <option key={index} value={index}>
                  {val.name}
                </option>
              )
            })
          : null}
      </select>
    </>
  )
}

export {Region}
