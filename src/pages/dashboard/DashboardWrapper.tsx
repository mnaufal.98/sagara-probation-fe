/* eslint-disable jsx-a11y/anchor-is-valid */
import {FC} from 'react'
import {useIntl} from 'react-intl'
import {PageTitle} from '../../_metronic/layout/core'
import InputWithIcon from '../../components/form-icon/FormIcon'
import { Region } from './region'

const DashboardPage: FC = () => (
  <>
  </>
)

const DashboardWrapper: FC = () => {
  const intl = useIntl()
  return (
    <>
      <PageTitle breadcrumbs={[]}>{intl.formatMessage({id: 'MENU.DASHBOARD'})}</PageTitle>
      <DashboardPage />
      {/* <InputWithIcon icon="fa-user" placeholder="Small icon" className="input-sm" />




      <h5>Component</h5> */}
      <Region />
    </>
  )
}

export {DashboardWrapper}
